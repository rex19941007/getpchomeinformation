# -*- coding: utf-8 -*-
"""
Created on Tue Apr  9 19:05:29 2019

@author: SONJI
"""

import base64
import urllib.parse
import requests
from bs4 import BeautifulSoup as bs  #取得HTML標籤
from fake_useragent import UserAgent #用戶代理

class main:
    def __init__(self):
        #使用者在此輸入關鍵字
        keyword = input('請輸入商品名稱:') #將使用者輸入後的資訊存入x變數裡
        main.getPChomeInformation(keyword)
       
    def getPChomeInformation(keyword):
        #將關鍵字轉碼，先轉成有百分比的utf8
        keyword_utf8 = urllib.parse.quote(keyword).encode('UTF-8')
        #再轉乘PChome搜尋的base64
        keyword_base64 = base64.b64encode(keyword_utf8)
        #轉成字串後取得base64的關鍵字內容(捨去開頭的b'跟結尾的')
        keyword = str(keyword_base64)[2:-1]
        #得到搜尋的url
        url = 'https://www.pcstore.com.tw/adm/psearch.htm?store_k_word='+keyword+'&slt_k_option=1'
        #建立連線
        request = main.url_open(url)
        #取得所有商品資訊
        all_goods = request.select('.pic2')
        for item in all_goods:
            title = item.select('a')[0].text
            print(title)
        
    def url_open(target):
        #建立連線
        requests_session = requests.session()
        #用戶代理
        user_agent = UserAgent()
        #設定header
        headers = {'user-agent': user_agent.random}
        #主頁面網址
        url_main = target
        #連線並且編碼(有亂碼)
        getRequests = requests_session.get(url_main, headers=headers)
        getRequests.encoding = 'big5'
        #使用BeautifulSoup取得HTML標籤 
        html_data = bs(getRequests.text,'html.parser')
        return html_data
        
if __name__ == '__main__':
    main()  #執行主程式